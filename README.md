# Sender / Receiver Proof Of Concept

This projects has one sender, and one receiver.
 - The Sender app sends groups of 4 messages. 
 - The Receiver app starts 3 listeners, that consume messages.

Each listener checks that he receives all messages of a group,
then it displays that the group has been consumed. If a group
is not complete - i.e. all messages of the group are not distributed
to the same client - an error message is displayed.

To run the sender :  
mvn -PSender deploy

To run the Receiver :  
mvn -PReceiver deploy

It is more interesting to start first the receiver, and to run the sender
in a different console.
