/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package top.marchand.java.jms.test.grouping.delivery.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

/**
 *
 * @author cmarchand
 */
public class StarterUtilities {
    
    public static final String PROP_BROKER_URL = "broker.url";
    public static final String PROP_BROKER_PRINCIPAL = "broker.user";
    public static final String PROP_BROKER_PASSWORD = "broker.password";

    public static Properties loadProperties(String propertyFileUrl) throws IOException {
        Properties props = new Properties();
        try {
            props.loadFromXML(new URL(propertyFileUrl).openStream());
            return props;
        } catch(IOException ex) {
            props.setProperty(PROP_BROKER_URL, "tcp://defineIt:61616");
            props.setProperty(PROP_BROKER_PRINCIPAL, "user");
            props.setProperty(PROP_BROKER_PASSWORD, "password");
            try {
                File f = new File(new URI(propertyFileUrl));
                props.storeToXML(new FileOutputStream(f), null);
                System.err.println("Vous devez compléter le fichier "+f.getAbsolutePath()+" puis relancer.");
            } catch(Throwable t) {
                // ignore
            }
            throw ex;
        }
    }
}
