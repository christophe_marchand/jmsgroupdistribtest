/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package top.marchand.java.jms.test.grouping.delivery;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.Stack;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import top.marchand.java.jms.test.grouping.delivery.services.JmsUtilities;
import top.marchand.java.jms.test.grouping.delivery.services.StarterUtilities;

/**
 *
 * @author cmarchand
 */
public class Receiver {

    public static void main(String[] args) throws Exception {
        Properties props = StarterUtilities.loadProperties(args[0]);
        String url = props.getProperty(StarterUtilities.PROP_BROKER_URL);
        String principal = props.getProperty(StarterUtilities.PROP_BROKER_PRINCIPAL);
        String password = props.getProperty(StarterUtilities.PROP_BROKER_PASSWORD);
        ReceiverThread r1 = new ReceiverThread(url, principal,password, "R1");
        ReceiverThread r2 = new ReceiverThread(url, principal,password, "R2");
        ReceiverThread r3 = new ReceiverThread(url, principal,password, "R3");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Press <ENTER> to stop listeners.");
        br.readLine();
        r1.close();
        r2.close();
        r3.close();
        System.exit(0);
    }

    static class ReceiverThread {
        private final String name;
        private final HashMap<String,Stack<String>> elements;
        private final QueueSession session;
        private final QueueReceiver rcv;

        public ReceiverThread(String brokerUrl, String brokerPrincipal, String brokerPassword, String name) throws Exception {
            super();
            this.name=name;
            elements = new HashMap<>();
            session = JmsUtilities.getQueueSession(
                    JmsUtilities.createBinding(brokerUrl,
                            brokerPrincipal,
                            brokerPassword));
            rcv = session.createReceiver(session.createQueue(JmsUtilities.QUEUE_NAME));
            rcv.setMessageListener((Message msg) -> {
                try {
                    String groupId = msg.getStringProperty("JMSXGroupID");
                    if(msg.propertyExists("MARKER")) {
                        switch(msg.getStringProperty("MARKER")) {
                            case "END" : {
                                Stack<String> stack = elements.get(groupId);
                                if(stack==null) {
                                    System.err.println(groupId+": END without start");
                                } else if(stack.size()<2) {
                                    System.err.println(groupId+": END with "+stack.size()+" messages");
                                } else {
                                    elements.remove(groupId);
                                }
                                System.out.println(groupId+" consumed by "+name);
                                break ;
                            }
                            case "START" : {
                                Stack<String> stack = new Stack<>();
                                elements.put(groupId, stack);
                                break;
                            }
                        }
                    } else {
                        TextMessage txtMsg = (TextMessage)msg ;
                        Stack<String> stack = elements.get(groupId);
                        if(stack==null) {
                            System.err.println(groupId+": MSG without start");
                        } else {
                            stack.push(txtMsg.getText());
                        }
                    }
                    msg.acknowledge();
                } catch(JMSException ex) {
                    ex.printStackTrace(System.err);
                }
            });
        }
        
        public void close() throws JMSException {
            rcv.close();
            session.close();
            System.out.println(name+" stopped");
        }
        
    }
}
