/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package top.marchand.java.jms.test.grouping.delivery.services;

import java.util.HashMap;
import java.util.Properties;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author cmarchand
 */
public class JmsUtilities {

    static final String CONNECTION_FACTORY_ENTRY = "connectionFactoryNames";
    static final String CONNECTION_FACTORY_QUEUE = "queueCF";
    static final String CONNECTION_FACTORY_TOPIC = "topicCF";
    static final String CONNECTION_FACTORY_NAMES = CONNECTION_FACTORY_QUEUE + ", " + CONNECTION_FACTORY_TOPIC;
    
    public static final String QUEUE_NAME = "TESTS.grouping.queue";

    public static Context createBinding(String brokerUri, String brokerAuthPrincipal, String brokerAuthPassword) throws NamingException {
        Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        env.put(Context.PROVIDER_URL, brokerUri);
        env.put(Context.SECURITY_PRINCIPAL, brokerAuthPrincipal);
        env.put(Context.SECURITY_CREDENTIALS, brokerAuthPassword);
        env.put(CONNECTION_FACTORY_ENTRY, CONNECTION_FACTORY_NAMES);
        Context ctx = new InitialContext(env);
        return ctx;
    }
    
    public static QueueSession getQueueSession(Context context) throws NamingException, JMSException {
        QueueConnectionFactory queueConnectionFactory = (QueueConnectionFactory)context.lookup(CONNECTION_FACTORY_QUEUE);
        QueueConnection queueCon = queueConnectionFactory.createQueueConnection();
        QueueSession queueSession = queueCon.createQueueSession(false, Session.CLIENT_ACKNOWLEDGE);
        queueCon.start();
        context.close();
        return queueSession;
    }
    
}
