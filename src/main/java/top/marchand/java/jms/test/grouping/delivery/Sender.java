/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package top.marchand.java.jms.test.grouping.delivery;

import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.QueueSession;
import top.marchand.java.jms.test.grouping.delivery.services.JmsUtilities;
import top.marchand.java.jms.test.grouping.delivery.services.StarterUtilities;

/**
 *
 * @author cmarchand
 */
public class Sender {

    public static void main(String[] args) throws Exception {
        System.out.println("Press <ENTER> to stop sending.");
        Properties props = StarterUtilities.loadProperties(args[0]);
        QueueSession session = JmsUtilities.getQueueSession(
                JmsUtilities.createBinding(
                        props.getProperty(StarterUtilities.PROP_BROKER_URL),
                        props.
                                getProperty(
                                        StarterUtilities.PROP_BROKER_PRINCIPAL),
                        props.getProperty(StarterUtilities.PROP_BROKER_PASSWORD)));
        MessageProducer producer = session.createProducer(session.createQueue(
                JmsUtilities.QUEUE_NAME));
        int available = System.in.available();
        while(available==0) {
            sendMessageGroup(session, producer, UUID.randomUUID().toString());
            TimeUnit.SECONDS.sleep(2);
            available = System.in.available();
        }
        byte[] b = new byte[available];
        System.in.read(b);
        session.close();
        System.exit(0);
    }

    public static void sendMessage(MessageProducer producer, Message msg, String groupId) throws JMSException {
        msg.setStringProperty("JMSXGroupID", groupId);
        producer.send(msg);
    }

    public static void sendMarkerMessage(QueueSession session, MessageProducer producer, String marker, String groupId) throws JMSException {
        BytesMessage msg = session.createBytesMessage();
        msg.setStringProperty("MARKER", marker);
        sendMessage(producer, msg, groupId);
    }

    public static void sendTextMessage(QueueSession session, MessageProducer producer, String txt, String groupId) throws JMSException {
        sendMessage(producer, session.createTextMessage(txt), groupId);
    }

    public static void sendMessageGroup(QueueSession session, MessageProducer producer, String groupId) throws JMSException {
        System.out.print("Sending "+groupId+"...");
        sendMarkerMessage(session, producer, "START", groupId);
        sendTextMessage(session, producer, "blabla 1", groupId);
        sendTextMessage(session, producer, "blabla 2", groupId);
        sendMarkerMessage(session, producer, "END", groupId);
        System.out.println(" Done.");
    }
}
